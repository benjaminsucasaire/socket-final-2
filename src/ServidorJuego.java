import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ServidorJuego
{
    //creamos las variables de tipo serversocket,socket,etc
    private ServerSocket yo;
    private Socket cliente;
    private int puerto;

    // objeto para enviar un mensaje
    private ObjectOutputStream emisor;
    //objeto para el receptor de mensaje
    private ObjectInputStream receptor;


    public ServidorJuego(int puerto){
        this.puerto=puerto;
        boolean escuchando = true;
        Random r=new Random();
        int numero=r.nextInt(1000);

        int numJugadores;
        //Aqui se crea un vector de direcciones Ip y puertos de los jugadores (vector de datos)
        //bash colocar en un metodo    (aqui)
        System.out.println("El numero generado es: "+ numero); //impresion temporal

        // bash metodo para ingresar el numero de jugadores  (aqui)
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el numero máximo de jugadores:");
        numJugadores = sc.nextInt();
        sc.close();

        ExecutorService servicio= Executors.newFixedThreadPool(numJugadores);
        Future <Paquete> resultado;
        try
        {
            //iniciar serversocket (verificar el puerto como parametro)
            yo = new ServerSocket(5002);

            while(escuchando)
            {
                cliente = yo.accept();
                System.out.println("Ya se conecto un cliente desde: " +
                        cliente.getInetAddress().getHostName() + ", puerto: "+cliente.getPort());
                resultado= servicio.submit(new AtiendeJuego(cliente,numero));

            }

            servicio.shutdown();
            yo.close();
        }

        catch (Exception e)
        {
            System.out.println("Ocurrio un error del tipo: "+e);
        }

    }


    //funcion para enviar un mensaje, el cual va recivir como parametro un objeto
    public void enviar(Object mensaje){
        try {
            emisor.writeObject(mensaje);
        } catch (IOException e) {
            System.out.println("No se puedo enviar "+e.getMessage());
        }
    }

    //funcion para recibir un mensaje de tipo estring
    public Object recibir(){
        Object salida="";
        try {
            salida= receptor.readObject();
        } catch (Exception e) {
            System.out.println("No se puedo recibir "+e.getMessage());
        }
        return salida;
    }



    public static void main(String arg[])
    {
        ServidorJuego s=new ServidorJuego(5000);


    }
}

