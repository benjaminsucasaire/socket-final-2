
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.Callable;

class AtiendeJuego implements Callable<Paquete> // Se puede cambiar al Thread
{
    Socket cliente;
    InetAddress nombreyDirIP;
    ObjectInputStream entrada;
    ObjectOutputStream salida;
    Paquete paqueteSalidaFinal;
    //Datos del paquete;
    int jugada;
    String nombreCli;
    int num_generado;
    long tiempo;
    String mensaje;
    boolean estadoJuego=false;
    public AtiendeJuego(Socket cliente, int num_generado)
    {
        this.cliente 				= cliente;
        this.num_generado	= num_generado;
        nombreyDirIP 			= cliente.getInetAddress();

    }

    //public void run()
    public Paquete call()
    {
        do
        {
            try {

                entrada = new ObjectInputStream(cliente.getInputStream());
                salida = new ObjectOutputStream(cliente.getOutputStream());

                Paquete paqueteSalida=new Paquete();
                Paquete paqueteLlegada=new Paquete();
                paqueteLlegada = (Paquete) entrada.readObject();

                //Desempaquetamos lo que llego
                jugada		=	paqueteLlegada.getJugada();
                nombreCli= paqueteLlegada.getNombre();
                tiempo		= paqueteLlegada.getTiempo();
                System.out.println("----- Se conecto el cliente: " + nombreCli +"-----");
                System.out.println("Juega con el número: "+jugada+"\n");

                mensaje = evaluar(jugada);

                if(mensaje.equals("Bingo Ganaste"))
                {
                    //datos para este
                    estadoJuego=true;
                    //datos del paquete para el servidor
                    paqueteSalida.setEstado(true); //Ganador
                    paqueteSalida.setNombre(nombreCli);
                    paqueteSalida.setJugada(jugada);
                    paqueteSalida.setTiempo(tiempo);
                    paqueteSalida.setMensaje(mensaje);
                    salida.writeObject(paqueteSalida);
                    break;
                }
                else
                {
                    paqueteSalida.setMensaje(mensaje);
                    salida.writeObject(paqueteSalida);
                }

            } catch (IOException | ClassNotFoundException e) {
                // TODO Auto-generated catch block
                System.out.println("Error en el paquete");
                e.printStackTrace();
            }
        }while(jugada !=-1);// -1=salir del juego

        try {
            entrada.close();
            cliente.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Se desconecto el jugador: "+ nombreCli +" ("+nombreyDirIP+")");
        if(estadoJuego)//si hay ganador
        {
            System.out.println("\t******* HAY UN GANADOR ********");
            System.out.println("\t El jugador: "+nombreCli +"\n");
        }
        return paqueteSalidaFinal;
    }
    private String evaluar(int num)
    {
        String mensaje_numero="";
        if (num>num_generado)
        {
            mensaje_numero="Intente con un numero mas pequeño";
        }
        if(num<num_generado)
        {
            mensaje_numero="Intente con un numero mas grande";
        }
        if (num==num_generado)
        {
            mensaje_numero="Bingo Ganaste";
        }
        return mensaje_numero;
    }
}

