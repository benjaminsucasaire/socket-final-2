import java.io.Serializable;

public class Paquete implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String nombre=""; 			//nombre del jugador
    private long tiempo=0; 				//Tiempo que duró el juego del ganador
    private int jugada;						//El número que juega el jugador
    private boolean estado=false;	//false= no hay ganador
    private String mensaje="Ningún Mensaje";
    public String getNombre() {
        return nombre;
    }
    public long getTiempo() {
        return tiempo;
    }
    public int getJugada() {
        return jugada;
    }
    public boolean isEstado() {
        return estado;
    }
    public String getMensaje() {
        return mensaje;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setTiempo(long tiempo) {
        this.tiempo = tiempo;
    }
    public void setJugada(int jugada) {
        this.jugada = jugada;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}

