
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class ClienteJugador
{
   private Socket yo = null;
    //variables para la conexion con el servidor
    private String ip;
    private  int puerto;
    //PrintWriter alServidor
    private ObjectOutputStream alServidor;
    //DataInputStream delServidor;
    private  ObjectInputStream delServidor;
    private  int numTecleado;
    private String nombreJugador;

    public ClienteJugador(String ip, int puerto)  {
        this.ip = ip;
        this.puerto = puerto;
        try
        {
            yo = new Socket(ip,puerto);//puerto e Ip del Servidor

            Scanner sc = new Scanner(System.in);
            System.out.println("Ingrese el nombre del jugador");
            nombreJugador = sc.next();


            System.out.println("========Juego: Adivina el número que genero el servidor ========");

            int intentos=0;
            long tiempoInicial=System.currentTimeMillis();
            long tiempoFinal;

            do{
                alServidor=new ObjectOutputStream(yo.getOutputStream());
                delServidor=new ObjectInputStream(yo.getInputStream());

                Paquete paqueteCliente=new Paquete();
                Paquete paqueteServidor=new Paquete();

                System.out.println("Intento número: "+ intentos +"\n Ingresa un Numero del 0 al 1000: ");
                numTecleado = sc.nextInt();

                tiempoFinal=System.currentTimeMillis();
                Long tiempo=(tiempoFinal-tiempoInicial)/1000; //tiempo transcurrido hasta la última jugada

                paqueteCliente.setJugada(numTecleado);
                paqueteCliente.setNombre(nombreJugador);
                paqueteCliente.setTiempo(tiempo);

                //alServidor=new ObjectOutputStream(yo.getOutputStream());
                alServidor.writeObject(paqueteCliente);

                //delServidor=new ObjectInputStream(yo.getInputStream());
                paqueteServidor =(Paquete) delServidor.readObject();
                String mensajeDelServidor = paqueteServidor.getMensaje();

                if(mensajeDelServidor.equals("Bingo Ganaste"))
                {
                    System.out.println("==========BINGO=========");
                    System.out.println("El Servidor dice:  "+ mensajeDelServidor);
                    System.out.println("En "+ (intentos) +" Intentos");
                    System.out.println("y en  "+paqueteServidor.getTiempo() + " segundos");
                    System.out.println("========================");
                    break;
                }
                else
                {
                    System.out.println("El Servidor dice:  No es el numero, "+paqueteServidor.getMensaje()+"\n");
                }
                intentos ++;

            }while(numTecleado!=-1); //-1 = terminar el juego

            sc.close();
            delServidor.close();
            alServidor.close();
            yo.close();
        }
        catch
        (Exception e)
        {
            System.out.println("Ocurrio un error de comunicación entre el cliente y el servidor");
            System.out.println(e);
        }
    }

    //funcion para enviar un mensaje, el cual va recivir como parametro un objeto
    public void enviar(Object mensaje){
        try {
            alServidor.writeObject(mensaje);
        } catch (IOException e) {
            System.out.println("No se puedo enviar "+e.getMessage());
        }
    }

    //funcion para recibir un mensaje de tipo estring
    public Object recibir(){
        Object salida="";
        try {
            salida= delServidor.readObject();
        } catch (Exception e) {
            System.out.println("No se puedo recibir "+e.getMessage());
        }
        return salida;
    }

    public static void main(String[] args)
    {
        ClienteJugador s=new ClienteJugador("localhost",5000);
    }
}

