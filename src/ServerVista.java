import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ServerVista  extends JFrame{
    private JTextArea textArea1;
    private JPanel mainPanel;
    private JButton ServerButton;
    private JButton clienteButton;
    //creamos una variable de tipo objeto servidor
    private ServidorJuego servidor;

    //creamos una variable de tipo cliente objeo
    private ClienteJugador cliente;
    private boolean soyServidor;
    ///////////////////////////////
    public void recibirMensajes(){
        //creamos hilo
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    //condicional si es o no el servidor
                    if (soyServidor){
                        // si soy servidor, recivir mensaje
                        Paquete ms= (Paquete) servidor.recibir();
                        //textArea1.append("Amigo: "+ms.toString()+"\n");
                        textArea1.append("Amigo: 1"+"\n");
                    }else {
                        Paquete ms=(Paquete) cliente.recibir();
                        //textArea1.append("Amigo: "+ms.toString()+"\n");
                        textArea1.append("Amigo: 2"+"\n");
                    }
                }
            }
        }).start();
    }


    ///////////////////
    public ServerVista(){
        setContentPane(mainPanel);
        setTitle("Servidor controlador");
        setSize(600,650);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        ServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                servidor = new ServidorJuego(5002);
                soyServidor=true;
                //llamamos a la funcion
                recibirMensajes();
            }
        });
        clienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                cliente=new ClienteJugador("localhost",5002);
                soyServidor=false;
                //llamamos a la funcion
                recibirMensajes();
            }
        });
    }

    public static void main(String[] args) {
        ServerVista myframe=new ServerVista();

        //myframe.setContentPane(new ServerVista().mainPanel);
        //myframe.setSize(750,700);
        //myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //myframe.setVisible(true);
        //myframe.pack();

    }
}
